from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    id = models.PositiveIntegerField(primary_key=True)
    email = models.EmailField(unique=True)
