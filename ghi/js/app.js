function createCard(name, description, pictureUrl, startdate, enddate) {
    return `
      <div class="card shadow mb-3">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">Starts: ${startdate} || Ends: ${enddate}</div>
      </div>
    `;
  }


// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';
  
//     try {
//       const response = await fetch(url);
  
//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//         console.log("cannot find detail");
//       } else {
//         const data = await response.json();
  
//         const conference = data.conferences[0];
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;
//         console.log(conference)
        
//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);

 
        
//         if (detailResponse.ok) {
//             const details = await detailResponse.json();
//             console.log(details);
//             const descTag = document.querySelector('.card-text');
//             descTag.innerHTML = details.conference.description;
//             const imageTag = document.querySelector('.card-img-top');
//             imageTag.src = details.conference.location.picture_url;
//         }
//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised

//     }
  
//   });

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
  
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            // console.log(name)
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startdate = details.conference.starts;
            const enddate = details.conference.ends;
            // console.log(startdate);
            // These variables remove the trailing time if the time is set to 0
            const lostStartTime = startdate.replace('T00:00:00+00:00', '');
            const lostEndTime = enddate.replace('T00:00:00+00:00', '');
            //
            const html = createCard(name, description, pictureUrl, lostStartTime, lostEndTime);
            const column = document.querySelector('.col');
            column.innerHTML += html;
          }
        }
  
      }
    } catch (e) {
      // Figure out what to do if an error is raised
    }
  
  });