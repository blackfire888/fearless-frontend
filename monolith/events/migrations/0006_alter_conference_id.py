# Generated by Django 4.0.3 on 2022-12-01 18:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0005_alter_location_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='conference',
            name='id',
            field=models.PositiveIntegerField(primary_key=True, serialize=False),
        ),
    ]
